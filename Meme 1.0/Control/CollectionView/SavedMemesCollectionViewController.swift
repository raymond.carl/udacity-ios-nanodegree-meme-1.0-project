//
//  SaveMemesCollectionViewController.swift
//  Meme 1.0
//
//  Created by Raymond Carl on 30/05/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import Foundation
import UIKit


class SentMemesCollectionViewController: UICollectionViewController {
    
    
    
    // The method in MemeCollectionViewCell

    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async{
    
            self.collectionView!.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let space:CGFloat = 3.0
        let dimension = (view.frame.size.width - (2 * space)) / 3.0

        flowLayout.minimumInteritemSpacing = space
        flowLayout.minimumLineSpacing = space
        flowLayout.itemSize = CGSize(width: dimension, height: dimension)
    }
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         
         return appDelegate.memes.count
         
     }

    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MemeDetailViewCell", for: indexPath) as! MemeCollectionViewCell
        let memeData = appDelegate.memes[(indexPath as NSIndexPath).row]

        // Set the name and image
        //cell.nameLabel.text = villain.name
        cell.imageView?.image = memeData.memedImage

        return cell
        
    }
    
}


    
    
    

