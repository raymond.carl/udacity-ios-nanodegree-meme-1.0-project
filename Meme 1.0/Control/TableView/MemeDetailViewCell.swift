//
//  MemeDetailViewCellTableViewCell.swift
//  Meme 1.0
//
//  Created by Raymond Carl on 05/06/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import UIKit

class MemeDetailViewCell: UITableViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewWillappear(_ animated: Bool){
        super.viewWillappear(animated)
        
        self.imageView!.image = UIImage(named: memedImage.imageName)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
