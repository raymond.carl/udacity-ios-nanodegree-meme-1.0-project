//
//  ViewController.swift
//  test
//
//  Created by Raymond Carl on 03/04/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//


// Moved this outside the ViewController class for easier access by the AppDelegate


import UIKit

class ViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate  {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bottomText: UITextField!
    @IBOutlet weak var topText: UITextField!
    @IBOutlet weak var cameraButton: UIBarButtonItem!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var navbar: UINavigationBar!
    
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    var dataController:DataController!

    let textDel = textDelegate()

    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Putting some of the textfield settings in the delegate was challenging, and those were placed in the formatText function
        formatText(textField: self.bottomText,  indicator: "BOTTOM")
        formatText(textField: self.topText,  indicator: "TOP")
        
        self.bottomText.delegate = textDel
        self.topText.delegate = textDel
        
        shareButton.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        subscribeToKeyboardNotifications()
        cameraButton.isEnabled = UIImagePickerController.isSourceTypeAvailable(.camera)
        

            

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromKeyboardNotifications()
    }
    
    @IBAction func pickImage(_ sender: Any) {
        self.shareButton.isEnabled = true
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
        
    }

    // Controller for the inbuilt UIImagePickerController function
    // MARK: imagePickerController
    func imagePickerController(_: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){

        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
            self.imageView.contentMode = .scaleAspectFit
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pickAnImageFromAlbum(_ sender: Any) {
        pickImage(UIImagePickerController.SourceType.photoLibrary)
    }
    
    @IBAction func pickAnImageFromCamera(_ sender: Any) {
        pickImage(UIImagePickerController.SourceType.camera)
    }

    func imagePickerControllerDidCancel(_: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        if bottomText.isFirstResponder {
            view.frame.origin.y = -getKeyboardHeight(notification)
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        view.frame.origin.y = 0
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }
    
    // This method generates the meme image, sends it to the UI Activity Controller, and saves it when the user clicks Share.
    // MARK: shareMeme
    @IBAction func shareMeme(_ sender: Any) {
        var memeImage = generateMemedImage()
        let uiController = UIActivityViewController(activityItems: [memeImage], applicationActivities: nil)
        present(uiController, animated: true, completion: nil)
        
        uiController.completionWithItemsHandler = {(activityType, completed, returnedItems, error) in
            
            if completed == true {
                print ("Success")
                self.save(image: memeImage)
                self.dismiss(animated: true, completion: nil)
                
            // If the user cancells before sharing, the UI Activity View Controller is dismissed without doing anything.
            }else if completed == false{
                self.dismiss(animated: true, completion: nil)
            }
            
            // Note:  Error handling has not been added.
        }
    }
    // MARK:  Save
    func save(image: UIImage) {
        // Create the meme
        let meme = Meme(topText: topText.text!, bottomText: bottomText.text!, originalImage: imageView.image!, memedImage: image)
        
        // Save to the app delegate shared object
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.memes.append(meme)
        print("saved!")
        
        
    }
    
    func generateMemedImage() -> UIImage {
        
        // TODO: Hide toolbar and navbar
        toolbar.isHidden = true
        navbar.isHidden = true
        
        // Render view to an image
        UIGraphicsBeginImageContext(self.view.frame.size)
        view.drawHierarchy(in: self.view.frame, afterScreenUpdates: true)
        let memedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        // TODO: Show toolbar and navbar
        toolbar.isHidden = false
        navbar.isHidden = false
        
        return memedImage
    }

    // This function clears everything if the user clicks the cancel button
    // MARK: Cancel
    @IBAction func userCancel(_ sender: Any) {
        
        formatText(textField: self.bottomText, indicator: "BOTTOM")
        formatText(textField: self.topText, indicator: "TOP")
        
        /* imageview needs to be unwrapped because the value is nil before the user selects an image.  This creates an error
        if the user hits cancel after creating the text inputs but before selecting an image. */
        if let imageView = imageView{
            imageView.removeFromSuperview()
        }
        
    }
    
    func formatText(textField: UITextField,  indicator: String){

        textField.text = indicator
        textField.textAlignment = .center
    }
    
}

