//
//  CollectionViewCell.swift
//  Meme 1.0
//
//  Created by Raymond Carl on 30/05/2020.
//  Copyright © 2020 Raymond Carl. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VillainCollectionViewCell", for: indexPath) as! VillainCollectionViewCell
        let villain = self.allVillains[(indexPath as NSIndexPath).row]

        // Set the name and image
        cell.nameLabel.text = villain.name
        cell.villainImageView?.image = UIImage(named: villain.imageName)

        return cell
    }
}



